+++
title = 'Krijns lego sets'
date = {date}
+++

This is a list of all lego sets Krijn has, for future reference. Images on this page are from [brickset.com](https://www.brickset.com). At the bottom of this page are some instructions on [how to make a list like this yourself](#make-a-list-like-this-for-yourself).

{sets}

## Make a list like this for yourself
### Create a JSON file with set numbers
This will be just a simple json file with lego set numbers.
```json
[
    60389,
    ...
    60373
]
```
### Get the data
I used [a python script](https://gitlab.com/airlobsters/alexhame.link/-/blob/master/generators/lego.py) to create the markdown file. This script:

```python
resp = http.request("GET", f"https://brickset.com/sets?query={set_number}")
html = resp.data
soup = BeautifulSoup(html, features="html.parser")
image = soup.find("a", {"class": "highslide plain mainimg"})
image_url = image["href"]
ext = os.path.splitext(urlparse(image_url).path)[1]
image_filename = os.path.join(DEST_DIR, f"{set_number}{ext}")
name = soup.find("div", {"class": "highslide-caption"}).find(
    "h1", recursive=False
).text
```

- checks if the data was downloaded before
- downloads the set name, and image
- generates markdown

Special thanks to [kevinquinn](https://kevinquinn.fun/blog/creating-pages-from-data-programmatically-for-hugo/) for the inspiration.

LEGO, the LEGO logo, the Minifigure, and the Brick and Knob configurations are trademarks of the LEGO Group of Companies. ©2024 The LEGO Group.Brickset, the Brickset logo and all content not covered by The LEGO Group's copyright is, unless otherwise stated, ©1997-2024 Brickset ltd.