function hashCode(str) {
	let hash = 0;
	for (let i = 0; i < str.length; i++) {
		hash = ((hash << 5) - hash) + str.charCodeAt(i);
		hash |= 0; // Convert to 32bit integer
	}
	return hash;
}
function toBits(n) {
	let bits = [];
	let number = n;
	while (number > 0) {
		bits.unshift(number % 2);
		number = Math.floor(number / 2);
	}
	return bits;
}

function update_header() {
	let today = new Date();
	let userAgent = navigator.userAgent;
	let seed = Math.floor(Math.abs(hashCode(userAgent + today.toDateString()+ localStorage.getItem('theme'))))
	// Create the SVG string with multiple squares at random coordinates
	const rows = 7
	const cols = 7
	const width = 100
	const height = 62
	const padding = 10
	const bits = toBits(seed);
	let svgString = `<svg xmlns="http://www.w3.org/2000/svg" width="${(width + padding) * cols}" height="${(height + padding) * rows}">`;
	for (let i = 0; i < cols; i++) {
		for (let j = 0; j < rows; j++) {
			let color = "#00000000"
			if (bits[(rows * i + j) % bits.length]) {
				color = "#FFFFFF33"
			}
			svgString += `<rect x="${i * (width + padding)}" y="${j * (height + padding)}" width="${width}" height="${height}" style="fill:${color};" />`;

		}
	}
	svgString += '</svg>';
	let header_elem = document.getElementsByClassName('header');
	header_elem[0].style.backgroundImage = `url("data:image/svg+xml,${encodeURIComponent(svgString)}")`
}

// in Firefox we need to add a new css style with document.write rather than modifying the href
// of the existing one, otherwise the screen will flash white while loading on dark themes
var light_color = "#BDCFB5";
var dark_color = "#482728";
var theme_css_elem = document.getElementById('theme_css');
var theme_css_clone = theme_css_elem.cloneNode(false);
var theme_elem = document.getElementById('theme_color') 
var js_url = document.currentScript.src; // example: http://example.com/myhugo/js/theme-switcher.js
if(localStorage.getItem('theme') === 'dark'){
	theme_css_clone.href = new URL("../css/dark.css", js_url);
	theme_css_elem.remove();
	document.write(theme_css_clone.outerHTML);
	theme_elem.content = dark_color;
}else if(localStorage.getItem('theme') === 'light'){
	theme_css_clone.href = new URL("../css/light.css", js_url);
	theme_css_elem.remove();
	document.write(theme_css_clone.outerHTML);
	theme_elem.content = light_color;
}

window.addEventListener("load", function(event){update_toggle_button();}, false);

function update_toggle_button(){
	var elem = document.getElementById('theme_css');
	var button = document.getElementById('change-theme-button');
	button.style.display = "";
	if(elem.href.endsWith('light.css')){
		button.getElementsByTagName('img')[0].src = new URL('../images/theme-switcher-moon.svg', js_url);
	}else if(elem.href.endsWith('dark.css')){
		button.getElementsByTagName('img')[0].src = new URL('../images/theme-switcher-sun.svg', js_url);
	}
	update_header();
}

function toggle_css(){
	var theme_css_elem = document.getElementById('theme_css');
	var theme_color_elem = document.getElementById('theme_color');
	if(theme_css_elem.href.endsWith('light.css')){
		theme_color_elem.content = dark_color;
		theme_css_elem.href = new URL("../css/dark.css", js_url);
		localStorage.setItem('theme', 'dark');
	}else if(theme_css_elem.href.endsWith('dark.css')){
		theme_color_elem.content = light_color;
		theme_css_elem.href = new URL("../css/light.css", js_url);
		localStorage.setItem('theme', 'light');
	}
}

function toggle_theme(){
	toggle_css();
	update_toggle_button();
}
