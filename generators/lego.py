import os
import re
import json
import logging
from bs4 import BeautifulSoup
from urllib.parse import urlparse
import urllib3

http = urllib3.PoolManager()
from pydantic import BaseModel
from datetime import datetime, timezone

logging.basicConfig(level=logging.INFO)

TEMPLATE_FILE = "archetypes/lego.md"
DATA_FILE = "page-data/lego-krijn.json"
DEST_DIR = "content/posts/lego-krijn"

class LegoSet(BaseModel):
    number: int
    name: str
    image_url: str
    image_filename: str

    @staticmethod
    def scrape(set_number):
        resp = http.request("GET", f"https://brickset.com/sets?query={set_number}")
        html = resp.data
        soup = BeautifulSoup(html, features="html.parser")
        image = soup.find("a", {"class": "highslide plain mainimg"})
        image_url = image["href"]
        logging.info("found image %s for set %s", image_url, set_number)
        ext = os.path.splitext(urlparse(image_url).path)[1]
        image_filename = os.path.join(DEST_DIR, f"{set_number}{ext}")
        name = soup.find("div", {"class": "highslide-caption"}).find(
            "h1", recursive=False
        ).text
        logging.info("found name %s for set %s", name, set_number)
        l = LegoSet(
            number=set_number,
            name=name,
            image_url=image_url,
            image_filename=image_filename,
        )
        l.download_image()
        with open(os.path.join(DEST_DIR, f"{set_number}.json"), "w") as of:
            of.write(json.dumps(l.model_dump(),indent=2))
        return l
    
    @staticmethod
    def from_cache(set_number):
        with open(os.path.join(DEST_DIR, f"{set_number}.json"), "r") as of:
            return LegoSet.model_validate_json(of.read())

    def download_image(self):
        resp = http.request("GET", self.image_url)
        with open(self.image_filename, "wb") as img_file:
            img_file.write(resp.data)

    def to_markdown(self):
        return f"""## {self.name}
{{{{< figure src="{os.path.basename(self.image_filename)}" title="set number: {self.number}" >}}}}
"""


def main():
    with open(DATA_FILE, "r") as df:
        lego_set_numbers = json.load(df)
    try:
        os.mkdir(DEST_DIR)
    except FileExistsError:
        pass
    lego_sets = []
    for lego_set_number in lego_set_numbers:
        try:
            lego_sets.append(LegoSet.from_cache(lego_set_number))
            logging.info("got %d from cache", lego_set_number)
            continue
        except Exception as exc:
            logging.info("%d not in cache", lego_set_number)
        try:
            lego_sets.append(LegoSet.scrape(lego_set_number))
            logging.info("got %d from web", lego_set_number)
            continue
        except Exception as exc:
            logging.info("%d not on web", lego_set_number, exc_info=True)
    lego_sets.sort(key=lambda x: x.name)
    with open(TEMPLATE_FILE) as f:
        markdown_string = f.read()
    with open(os.path.join(DEST_DIR, "index.md"), "w") as of:
        result = re.sub(r"{sets}", " ".join([l.to_markdown() for l in lego_sets]), markdown_string)
        result = re.sub(r"{date}", datetime.now(timezone.utc).replace(microsecond=0).isoformat(), result)
        of.write(result)


if __name__ == "__main__":
    main()
