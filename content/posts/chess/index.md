+++
title = 'Chess'
date = 2024-08-08T14:18:13+02:00
+++

I've recently started playing chess with my son. He's just starting, so playing against him is very easy for me, even though I'm by no means a good chess player. In order to make the game a bit more fair, and thus more fun for both of us, I want to introduce a handicap for me. I found a table with handicaps[^kauf] in the [wikipedia chess handicap article](https://en.m.wikipedia.org/wiki/Handicap_(chess)). Based on this list I created the following 10 levels of play, where level 1 gives the player the most advantage.

I like the versions without giving one player multiple consecutive moves, so the game can start immediately as normal.

| level | Handicap                                       | Eval |
|-------|------------------------------------------------|------|
| 1     | Queen and pawn (remove d1 and f2)              | 8.8  |
| 2     | Queen (remove d1)                              | 7.95 |
| 3     | Rook, knight, and pawn (remove a1, b1, and f2) | 7.33 |
| 4     | Rook and knight (remove a1 and b1)             | 6.59 |
| 5     | Two knights (remove b1 and g1)                 | 5.64 |
| 6     | Queen for knight (remove d1 and b8)            | 5.21 |
| 7     | Rook and pawn (remove a1 and f2)               | 4.48 |
| 8     | Rook (remove a1)                               | 3.7  |
| 9     | Knight (remove b1 or g1)                       | 2.81 |
| 10    | Two pawns (remove c7 and f7)                   | 2.03 |

[^kauf]: Kaufman, Larry (January 2024). "Against All Odds". New in Chess. New in Chess. pp. 70–77.