+++
title = 'aiGrunn 2023'
date = 2023-11-13T21:42:37+01:00
tags = ['ai', 'conference', 'review']
+++
This year I visited [aiGrunn](https://aigrunn.org/). I got the tip to visit this event from a coworker. When looking online I immediately recognized the art style from [PyGrunn](https://pygrunn.org/). Overall the visual impression and style was very well executed. I really liked how they integrated the Groninger word for egg (ai) into the design and opening talk. The audience was asked to hold an imaginary egg and respond to a series of questions with "ai". The final question was a very relieving and self-aware "who's tired of these questions?"

{{< figure src="IMG_20231110_104855662.jpg" title="Emil Loer's talk about Real-time speech recognition" >}}

Most of the talks were about Large Language Models (LLM's) which is to be expected in the current year. There were some interesting talks nonetheless. I especially liked Emil Loer's talk about Real-time speech recognition. He talked about using LLM's in the context of drive-through restaurant order taking. I found the part about deciding where to run the different parts of the application. Where perform text to speech, do inference, generate the response audio etc. There is a balance between accuracy and response time. Response time during a conversation is very important because the user can easily get frustrated by a second of silence. I think there might be an application for my own work as well in the context of customer service calls.

Overall I would recommend this conference, and will try to visit again if they organize it next year.

