+++
title = 'Creating a Website'
date = 2023-11-01T21:58:46+01:00
lastmod = 2024-10-29T17:49:30+02:00
+++
Freedom has taken on a new dimension in the information age. The internet has transformed the way we interact, share information, and express our thoughts. It's a powerful tool that has the potential to connect us with a global audience, yet the concept of freedom in this digital world is not as straightforward as it may seem.

In the early days of the internet, often called Web 1.0, access to the digital realm was confined by one's technical prowess. In contrast, today's Web 2.0 era is marked by influential platforms making the internet accessible to a wider audience. However, this newfound accessibility brings its own set of constraints. These platforms, evolving over time, have adopted stricter content moderation and community guidelines. Though aimed at preserving order and safety, they have inadvertently curbed freedom of speech. These platforms, once synonymous with free expression, now dictate what's acceptable to say, prompting concerns about stifled diversity of opinion and the openness of digital discourse.

I want to create my own website to share my thoughts without being restricted by platforms that determine what can and cannot be said. Admittedly there are still major influences by the hosting service and search engines that influence the sharing of information, but this is at least a step towards digital autonomy.

This blog will be primarily the sharing of my thoughts, and also somewhat of a proof of concept to test the feasibility of managing the website yourself. I do not intend to write too controversial content on this blog, so I don't expect much issue in that regard.

## Web Bloat

Besides restriction of freedom there is another issue that impairs the sharing and especially the consuming of information on the web. In order to make writing websites financially feasible, there has to be some kind of revenue model behind it. The most often chosen model; advertisements, has many downsides.

### User experience

The experience for the user is worse, because the information they look for is mixed with ads they did not ask for.

### Infringed privacy

The users privacy is infringed by ad networks tracking their online activity. Most internet users have to actively block this to make browsing more convenient. Government regulations to attempt to combat this have failed miserably. Making cookie notifications mandatory has made browing the web outside your main platforms very painful, and not safer since the action to just accept all cookies is still the most convenient.

### Increased filesizes and load times

All this extra bloat takes extra time to load. JavaScript files for the trackers, animations and frameworks used that add very little to the information displayed.

### The solution

I think web developers should not actively hinder their users, but empower them. My objective with this site is to create a lightweight (as in filesize) site, that is easy to maintain for the web developer, and is easy to use. It should for example adhere to the [DRY principle](https://en.wikipedia.org/wiki/Don't_repeat_yourself) to make development easier, and should not store any personal information about the user.

## Tech

This website was made with [gitlab pages](https://gitlab.com/pages). It can host static websites for free, and provide certificates through [letsencrypt](https://letsencrypt.org/). I used [namecheap](https://www.namecheap.com/) for a cheap domain name, so that is the only cost at the moment. I use [hugo](https://gohugo.io/) as a static site generator, and the [no-js-hugo-theme](https://github.com/stevenengler/no-js-hugo-theme) template. The inspiration for the background for the header is [Hero Patterns](https://heropatterns.com/). I ended up creating a script that generates a unique svg image based on the user agent date, and theme, so it is different every day, on every device. The full source for this website is [available on gitlab here](https://gitlab.com/airlobsters/alexhame.link)
