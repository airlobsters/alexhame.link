+++
title = 'Krijns lego sets'
date = 2024-12-27T21:51:35+00:00
+++

This is a list of all lego sets Krijn has, for future reference. Images on this page are from [brickset.com](https://www.brickset.com). At the bottom of this page are some instructions on [how to make a list like this yourself](#make-a-list-like-this-for-yourself).

## Construction Excavator 
{{< figure src="60420.jpg" title="set number: 60420" >}}
 ## Custom Car Garage
{{< figure src="60389.jpg" title="set number: 60389" >}}
 ## Electric Sports Car
{{< figure src="60383.jpg" title="set number: 60383" >}}
 ## Fire Helicopter
{{< figure src="60318.jpg" title="set number: 60318" >}}
 ## Fire Rescue Boat
{{< figure src="60373.jpg" title="set number: 60373" >}}
 ## LEGO City Advent Calendar 2023
{{< figure src="60381.jpg" title="set number: 60381" >}}
 ## LEGO Friends Advent Calendar 2023
{{< figure src="41758.jpg" title="set number: 41758" >}}
 ## Monster Truck
{{< figure src="60402.jpg" title="set number: 60402" >}}
 ## Police Bike Car Chase
{{< figure src="60392.jpg" title="set number: 60392" >}}
 ## Police Car
{{< figure src="60312.jpg" title="set number: 60312" >}}
 ## Recycle Truck
{{< figure src="6668.jpg" title="set number: 6668" >}}
 ## Tow Truck Trouble
{{< figure src="60137.jpg" title="set number: 60137" >}}


## Make a list like this for yourself
### Create a JSON file with set numbers
This will be just a simple json file with lego set numbers.
```json
[
    60389,
    ...
    60373
]
```
### Get the data
I used [a python script](https://gitlab.com/airlobsters/alexhame.link/-/blob/master/generators/lego.py) to create the markdown file. This script:

```python
resp = http.request("GET", f"https://brickset.com/sets?query={set_number}")
html = resp.data
soup = BeautifulSoup(html, features="html.parser")
image = soup.find("a", {"class": "highslide plain mainimg"})
image_url = image["href"]
ext = os.path.splitext(urlparse(image_url).path)[1]
image_filename = os.path.join(DEST_DIR, f"{set_number}{ext}")
name = soup.find("div", {"class": "highslide-caption"}).find(
    "h1", recursive=False
).text
```

- checks if the data was downloaded before
- downloads the set name, and image
- generates markdown

Special thanks to [kevinquinn](https://kevinquinn.fun/blog/creating-pages-from-data-programmatically-for-hugo/) for the inspiration.

LEGO, the LEGO logo, the Minifigure, and the Brick and Knob configurations are trademarks of the LEGO Group of Companies. ©2024 The LEGO Group.Brickset, the Brickset logo and all content not covered by The LEGO Group's copyright is, unless otherwise stated, ©1997-2024 Brickset ltd.