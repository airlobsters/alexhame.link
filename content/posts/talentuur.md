+++
title = 'Talentuur'
date = 2025-01-27T16:07:35+01:00
lastmod = 2025-02-18T10:44:00+01:00
+++

I'm teaching a short course on programming and AI for middle schoolers. called a "talentuur", (Talent hour).

[link to the course]({{< ref "/talentuur" >}})

My goal with the talent hour is to introduce high school students to programming and AI. I choose the programming language Python for this because it is the language I have the most experience with, the syntax is easy for beginners to learn, and it is a widely used language for AI-related projects. I plan to reuse as much existing course material as possible. This is easier for me, but it also ensures that the students become familiar with several online platforms where you can learn for free.

ING allows me to teach this course during work hours as part of their [sustainability policy](https://www.ing.nl/de-ing/duurzaamheid/financieel-gezond).

## reflecting
One interesting thing happened while I was explaining print statements and comments. They were doing the following assignment:

```python 
# use comments to make sure only the last line is printed
print("this line should not be printed")
print("this line should also not be printed")
print("this line should be printed")
```
One of the things I noticed during the first lesson is that one of the students wrote:
```python 
# use comments to make sure only the last line is printed
print("this line should not be printed")
print("this line should also not be printed")
print("this line should be printed")
print("print only the third line")
```
I think I have to explain a bit more clearly the difference between using an LLM and programming. My first experience using computers was typing commands in MS-DOS to start up a game. If you did not type the correct command, the game would not start, so it was obvious to me you would have to provide very precise instructions when programming. For the kids I'm teaching now their experience is a bit different. They're used to typing or speaking to a computer in natural language, and it just understands them. 