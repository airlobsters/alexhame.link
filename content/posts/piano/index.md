+++
title = 'FOSS piano learning'
date = 2024-06-23T23:43:47+02:00
+++
Recently I've started to learn to play the piano. There are a lot of resources available to start learning, but where to start? I remembered seeing a video of a kind of game where you could learn to play the piano. [Synthesia](https://www.synthesiagame.com/) as I found out it is called, is unfortunately not free, and also does not really teach sight reading. Therefore I considered it more of a novelty than a real learning tool, however fun it looked.

# The instrument
The choice for me was between two entry-level pianos: the Roland FP-10 and the Yamaha P45. I ended up choosing the Roland, because according to many reviews it has the best key action. It has a USB-A connector on the back so you can connect it as a MIDI controller.
{{< figure src="roland-fp10-stand.jpg" title="The piano comes with a pedal. I got the stand with it as well." >}}

# Pianobooster
I use [pianobooster](https://www.pianobooster.org/) to learn to play. It's free and open source. With the installation come some beginner practice tracks. These tracks are easy to start with, and they have some tips for beginners on how to position your hands as well. It's fun to play along with the music, hearing some other notes besides your own to make it sound a little less horrible. {{< figure src="pianobooster-screenshot.png" title="The interface is basic, but works well" >}} You can practice both hands separately or at the same time. There is a mode where the program follows you, and waits until you play the right note. The other mode continues playing regardless, and usually works better once you can play the track a bit better. The follow you mode sometimes gets a bit annoying when it pauses for every missed note, but is great when first learning a new melody.

# MIDI files
There are a lot of midi files available for free on [bitmidi](https://bitmidi.com/). It has a lot of popular songs, and also classical pieces. I found a lot of midi files for psalms on this site: [www.hervormddinteloord.com](https://www.hervormddinteloord.com/muziek.htm). 