
+++
title = 'Talentuur'
date = 2025-01-08T17:13:19+01:00
menu = 'main'
+++

# Talentuur programmeren - AI

Wat je gaat leren:
- de basics van python
- een simpele AI maken die een spelletje kan spelen

Wat heb je nodig:
- enige affiniteit met het gebruik van computers
- een basisbegrip van de engelse taal
- een laptop (van school)

## planning
| datum                      | onderwerp             |
|:---------------------------|:----------------------|
| woensdag 12 februari 2025  | python basics 1       |
| woensdag 26 februari 2025  | simpele opdracht      |
| woensdag 5 maart 2025      | python basics 2       |
| woensdag 12 maart 2025     | opdracht: spelbord    |
| woensdag 19 maart 2025     | opdracht: de AI       |
| woensdag 26 maart 2025     | wedstrijd/ afsluiting |


### overige onderwerpen, afhankelijk van de tijd:
- [Codingame bot programming](https://www.codingame.com/multiplayer/bot-programming) 
- [coding for kids](https://codingforkids.io/en/signup) 
- Wat doet een data scientist?
- [roadmap for learning python](https://roadmap.sh/python) 


