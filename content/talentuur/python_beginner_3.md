+++
title = 'Python Basics 2'
date = 2025-01-27T13:58:45+01:00
draft = true
+++
De tweede les gaan we verder met verschillende python concepten. 

# python gevorderde concepten
- classes

## possible games

Tic-Tac-Toe: This classic game is simple to implement and provides a great introduction to AI concepts like decision-making and strategy. Kids can program an AI to play against them, learning about algorithms and game theory.

Rock, Paper, Scissors: Another simple game with clear rules. Kids can create an AI that learns from previous rounds to predict and counter the player's moves, introducing them to machine learning concepts.

Connect Four: This game is slightly more complex than Tic-Tac-Toe but still easy to understand. It allows kids to explore more advanced AI strategies and decision trees.

Hangman: Kids can program an AI to guess letters in a word, learning about probability and pattern recognition.