+++
title = 'Python Basics 2'
date = 2025-01-27T13:58:45+01:00
draft = true
+++
De tweede les gaan we verder met verschillende python concepten. 

[learn-python.adamemery.dev](https://learn-python.adamemery.dev/basics)

We waren gebleven bij Basics, die herhalen we kort, dan gaan we verder met
- Operators and Expressions
- Control Flow
- Functions
voor de eerste helft van het lesblok.

Play Durian

P - pak een kaart
L - accepteer links
R - accepteer rechts
B - bel
🍈
🍓
🍌
🍇