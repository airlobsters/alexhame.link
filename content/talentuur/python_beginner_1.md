+++
title = 'Python Basics'
date = 2025-02-12T10:00:00+01:00
+++

In deze les doen we introductie, en behandelen we python basics.

## introductie
De eerste les besteden we wat tijd aan introductie. Ik ben benieuwd wie jij bent en wat je ervaring is met programmeren. Wat verwacht je van dit talentuur? Daarnaast ga ik mezelf even voorstellen: wie ben ik en mijn werkgever ING. 

We staan stil bij de volgende vragen:
- Wat is programmeren?
- Wat is AI?
- Is er een simpel spelletje dat jullie leuk zouden vinden om de AI te laten spelen?

Als laatst geef ik een kort overzicht over hoe het [lesprogramma]({{< ref "/talentuur#planning" >}}) er uit ziet. 

## python basics
We beginnen met de basis van python aan de hand van een online (engelse) tool waar je interactief opdrachten kunt maken: [learn-python.adamemery.dev](https://learn-python.adamemery.dev/basics). Mijn doel is om in de eerste les deze 4 onderwerpen te behandelen:
- Basics
- Operators and Expressions
- Control Flow
- Functions

## opdracht
Als er tijd over is kunnen we alvast beginnen met het maken van het spel dat de AI kan spelen.
Daarna beginnen we met de python basics aan de hand van een [jupyter notebook](https://jupyter.org/try-jupyter/lab/)