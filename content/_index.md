---
title: Home
menu:
  main:
    weight: 100
---

# Hi, I'm Alex

I'm a data scientist and engineer with a background in artificial intelligence from the Rijksuniversiteit Groningen. I'm interested in open source software, Dungeons and Dragons and games in general. I live in Leeuwarden with [my wife](https://ingehame.link/) and kids.